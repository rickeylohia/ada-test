from flask import Flask, request, jsonify, abort
import sqlite3
import json
import re  # regular expressions
import logging

app = Flask(__name__)
DBPATH = "../database.db"


def fetch_all_messages():
    with sqlite3.connect(DBPATH) as conn:
        messages_res = conn.execute("select body from messages")
        messages = [m[0] for m in messages_res]
        return messages


def fetch_state_by_id(state_id):
    with sqlite3.connect(DBPATH) as conn:
        res = conn.execute(
            "select value from state where id=?", (state_id,)
        ).fetchall()
        return "" if len(res) == 0 else res[0][0]


"""
messages endpoint - Ideally it should have some search criteria or pagination config rather than returning all rows from DB 
and a maximum limit set that can be returned by it.

search endpoint - Ideally the query to fetch the answers should be chunked rather than getting all the rows in one go. 

Yes would expect the dataset to fit in the memory, as we are putting limits which and controlling how much data will be 
stored in the memory. And by making sure the data set fits in the memory we won't have rely on other forms of temp storage
(eg disk) as they are slower to R/W from and would increase the response time.
"""


@app.route("/messages", methods=["GET"])
def messages_route():
    """
    Return all the messages
    """
    try:
        messages = fetch_all_messages()
        state_map = {}
        result = []
        for message in messages:
            ids = set()
            [ids.add(state_id) for state_id in re.findall("(?<={)[^|]*(?=[|])", message)]
            for state_id in ids:
                if state_id not in state_map:
                    val = fetch_state_by_id(state_id)
                    state_map[state_id] = val
                if state_map[state_id] != "":
                    message = re.sub("[{]" + state_id + "[|][^}]*[}]", state_map[state_id], message)
                else:
                    message = re.sub("[{]" + state_id + "[|]([^}]*)[}]", r"\1", message)
            result.append(message)
        return jsonify(result), 200
    except Exception as e:
        logging.exception(e)
        return abort(400)


@app.route("/search", methods=["POST"])
def search_route():
    """
    Search for answers!

    Accepts a 'query' as JSON post, returns the full answer.

    curl -d '{"query":"Star Trek"}' -H "Content-Type: application/json" -X POST http://localhost:5000/search

    """
    try:
        query = request.get_json().get("query")
        if query is None or query.strip() == "":
            raise Exception("Query required")
        with sqlite3.connect(DBPATH) as conn:
            res = conn.execute(
                "select a.id, a.title, b.content from answers a, blocks b where a.id = b.answer_id",
            )
            answers = []
            for r in res:
                keywords = query.split()
                content = json.loads(r[2])
                reg_ex = '(' + ')|('.join(keywords) + ')'
                keyword_match = set(re.findall(reg_ex, r[1], re.I))
                for m in keyword_match:
                    reg_ex = re.sub('([(]' + ''.join(m) + '[)][|])|[|]{0,1}[(]' + ''.join(m) + '[)]$', "", reg_ex, 0,
                                    re.M | re.I)
                reg_ex = json_search(content, reg_ex)

                if reg_ex == '':
                    answers.append({"id": r[0], "title": r[1], "content": content})
            return jsonify(answers), 200
    except Exception as e:
        logging.exception(e)
        return abort(400)


def json_search(obj, reg_ex):

    def rec_search(obj, reg_ex):
        if reg_ex == '':
            return reg_ex
        if isinstance(obj, dict):
            for k, v in obj.items():
                if reg_ex == '':
                    break
                if isinstance(v, (dict, list)):
                    reg_ex = rec_search(v, reg_ex)
                elif k != 'type':
                    keyword_match = set(re.findall(reg_ex, str(v), re.I))
                    for m in keyword_match:
                        reg_ex = re.sub('([(]' + ''.join(m) + '[)][|])|[|]{0,1}[(]' + ''.join(m) + '[)]$', "", reg_ex,
                                        0, re.M | re.I)
        elif isinstance(obj, list):
            for item in obj:
                if reg_ex == '':
                    break
                reg_ex = rec_search(item, reg_ex)
        return reg_ex

    return rec_search(obj, reg_ex)


if __name__ == "__main__":
    app.run(debug=True)
